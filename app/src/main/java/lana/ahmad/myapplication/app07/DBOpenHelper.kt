package lana.ahmad.myapplication.app07

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context):SQLiteOpenHelper(context,DB_Name,null,DB_Ver){
    override fun onCreate(db: SQLiteDatabase?) {
        val tMhs = "create table mhs(nim text primary key,nama text,prodi text )"
        db?.execSQL(tMhs)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object {
        val DB_Name = "mahasiswa"
        val DB_Ver = 1
    }
}