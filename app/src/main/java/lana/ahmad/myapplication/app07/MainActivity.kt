package lana.ahmad.myapplication.app07

import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.View
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.zxing.BarcodeFormat
import com.google.zxing.integration.android.IntentIntegrator
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onClick(v: View?) {
            when(v?.id){
                R.id.btnScanQR -> {
                    intentIntegrator.setBeepEnabled(true).initiateScan()
                }
                R.id.btnGenerateQR -> {
                    val barCodeEncoder = BarcodeEncoder()
                    val bitmap = barCodeEncoder.encodeBitmap(edQrCode.text.toString(),BarcodeFormat.QR_CODE,400,400)
                    imV.setImageBitmap(bitmap)
                }
                R.id.btnSimpan->{
                dialog.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",insDataD)
                    .setNegativeButton("No",null)
                dialog.show()
            }
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        val intentResult = IntentIntegrator.parseActivityResult(requestCode,resultCode,data)
        if(intentResult!=null){
            edQrCode.setText(intentResult.contents)
            val strToken = StringTokenizer(edQrCode.text.toString(),";",false)
            edNIM.setText(strToken.nextToken())
            edNAMA.setText(strToken.nextToken())
            edPRODI.setText(strToken.nextToken())

        }
        else{
            Toast.makeText(this,"dibatalkan",Toast.LENGTH_SHORT).show()
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onStart() {
        super.onStart()
        showData()
    }

    fun showData(){
        var sql=""
        sql="select nim as _id, nama, prodi from mhs"
        val c : Cursor = db.rawQuery(sql,null)
        adapter = SimpleCursorAdapter(this, R.layout.item_data_mhs,c,
            arrayOf("_id", "nama", "prodi"), intArrayOf(R.id.tNim, R.id.tNama, R.id.tProdi),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        lvView.adapter = adapter
    }



    lateinit var db : SQLiteDatabase
    lateinit var intentIntegrator: IntentIntegrator
    lateinit var dialog : AlertDialog.Builder
    lateinit var adapter : ListAdapter
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        dialog = AlertDialog.Builder(this)
       intentIntegrator = IntentIntegrator(this)
        btnScanQR.setOnClickListener(this)
        btnGenerateQR.setOnClickListener(this)
        btnSimpan.setOnClickListener(this)
        db =DBOpenHelper(this).writableDatabase
    }
    fun getDbObject() : SQLiteDatabase{
        return db
    }

    fun insData(a:String,b:String,c:String){
        var cv : ContentValues = ContentValues()
        cv.put("nim",a)
        db.insert("mhs",null,cv)
        cv.put("nama",b)
        db.update("mhs",cv,"nim=$a",null)
        cv.put("prodi",c)
        db.update("mhs",cv,"nim=$a",null)
        edQrCode.setText("")
        edNIM.setText("")
        edNAMA.setText("")
        edPRODI.setText("")
        showData()
    }

    val insDataD = DialogInterface.OnClickListener { dialog, which ->
        insData(edNIM.text.toString(),edNAMA.text.toString(),edPRODI.text.toString())

    }
}
